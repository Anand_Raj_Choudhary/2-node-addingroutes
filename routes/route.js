const express=require("express");

const router=express.Router();

router.route("/").get((req,res)=>{
    res.status(200).json({Message :"GET ALL CONTACT"})
}
)
router.route("/").post((req,res)=>{
    res.status(200).json({Message :"CREATE CONTACT"})
})
router.route("/:id").get((req,res)=>{
    res.status(200).json({Message :`GET CONTACT for ${req.params.id}`})
}
)
router.route("/:id").put((req,res)=>{
    res.status(200).json({Message :`Update Contact for ${req.params.id}`})
}
)
router.route("/:id").delete((req,res)=>{
    res.status(200).json({Message :"DELETE CONTACT"})
}
)
module.exports=router