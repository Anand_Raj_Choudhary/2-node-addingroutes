const express =require("express");
require("dotenv").config();
const app=express();

app.use('/api/contacts',require("./routes/route"))
const port=process.env.port||5001;
app.listen(port,()=>{console.log(`Server started at ${port}`);})